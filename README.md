# CFAST YAML generation (with templates :D

Two-line Summary: It's really easy to make errors in a CFAST YAML manifest.
This tool uses an ansible template to make the required edits a little more human readable.

Eventually, the `key: pair` format that's easier to edit for humans is also easier to edit for scripts. With sed. So I hope I'll have that going for me, which is nice.

## Requirements

- python (probably 3)
- pip
- virtualenv
- wheel? maybe. Mine throws an error but then pip is smart and recovers to install a different way. If yours isn't quite as smart, try installing wheel at the system level.

## How To

1. Edit `vars/$platform.yml` with the versions/part numbers you need to update.
2. Run **The Commands**

## **The Commands** to Run

1. `autovenv.sh`
2. `source ansible_venv/bin/activate`
3. `ansible-playbook render-$platform.yml`

Generated YAML file will be in `output_manifests/`, named according to the parent part number variable that was provided in vars.

## TODO

- a330 vars file
- CLI/Interactive script to feed the vars files the commonly updated (i.e. non-third party and non-DAL-D) package details
